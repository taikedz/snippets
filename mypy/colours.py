"""
Use terminal ASCII colours (UNIX/Linux)

Example:


    import mypy.colours as clr

    print("".join([clr.red, clr.bold, "My text", clr.default]))

    # or
    
    cp = clr.ColourPrinter(clr.red, clr.bright)
    cp.print("My text")

    cp.print("Label", " value") # "Label" in the initialised colour, " value" in default colour
"""


def _a(num: str):
    return '\033[%sm' % num

# Dereferenceable colours

"Return the colours to the terminal's default"
default = _a("0")

black   = _a("30")
red     = _a("31")
green   = _a("32")
yellow  = _a("33")
blue    = _a("34")
purple  = _a("35")
teal    = _a("36")
white   = _a("37")

bright    = _a("1")
dim       = _a("2")
underline = _a("4")
invert    = _a("7")
strike    = _a("9")

dimbright_off  = _a("22")
highlight_off  = _a("49")
underline_off  = _a("24")
invert_off     = _a("27")
strike_off     = _a("29")

class ColourPrinter:
    """
    A convenience print utility to use colours in a UNIX/Linux terminal.
    """

    def __init__(self, *colours):
        """
        *colours : a series of colour flags to use with the printer
        """
        self.setColours(*colours)

    def setColours(self, *colours):
        """
        *colours : a series of colour flags to use with the printer
        """
        self.default_colour = "".join(colours)

    def print(self, colourtext, normaltext=''):
        """
        Print colourtext in the colour initialised with,
        and normaltext in the default terminal colour,
        on the same line.
        """
        print("".join([self.default_colour, colourtext, default, normaltext]))
