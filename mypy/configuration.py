import yaml

import mypy.jsonlib as jsonlib
import mypy.runner as runner

class Loader:

    def __init__(self, arguments: dict):
        configfile = arguments["config"]
        self.walker = None
        self.arguments = arguments

        if not arguments["config"]:
            return

        try:
            with open(arguments["config"], 'r') as fh:
                yamldata = yaml.load(fh)
                self.walker = jsonlib.JSONWalker(yamldata)

            self._extractMain()
        except FileNotFoundError:
            raise FileNotFoundError("Could not locate specified config file '%s'"%arguments["config"])

    def _extractMain(self):
        self.extractConfigs("main", ["show_commands", "dry_run"])

        runner.setShowCommands(self.arguments["show_commands"])

    def _extract(self, action: str, key: str):
        if not self.walker:
            return

        data = self.walker.get("%s/%s"%(action, key) )
        if data and (key not in self.arguments.keys() or not self.arguments[key]):
            # we have data and ( key not populated, or key not positive )
            self.arguments[key] = data

    def extractConfigs(self, action: str, keys: list):
        for key in keys:
            self._extract(action, key)
