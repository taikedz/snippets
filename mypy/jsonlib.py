import json
import re

class JSONWalker:
    """
    A class to walk JSON data using a simplified path notation.

    A path is a series of names separated by a '/', or other separator. No token can have the separator in its name.

        my/json/path

    Arrays must be accessed via a number
    '*' iterates over all array entries.

        cars/0/make  # returns a value (assuming make is a single value)
        cars/*/make  # returns a list of values

    Dictionnaries are accessed by name. Accessing a dictrionary by name directly returns the dictionary itself.
    '*' iterates over all names. Using '*' strips names, returning a list instead.

        cars/*/passengers     # returns a list of passenger dicts
        cars/*/passengers/*  # returns a list of values from each passenger dict property
    """

    def __init__(self, jsondata):
        """
        Create a new JSONWalker from a JSON dict/list object, or from a raw string of JSON data
        """
        if type(jsondata) == str:
            self.jsondata = json.loads(jsondata)
        else:
            self.jsondata = jsondata

    def get(self, path: str, separator='/', flatten=False):
        """
        Return all items found along the path. Path tokens are split along the separator.

        Normally for each wildcard used, a nested array is created. Specify `flatten=True` to attempt to flatten into a flat array.
        """
        path = path.split(separator)
        self.separator = separator
        self.flatten = flatten

        try:
            return self.__extract(path, self.jsondata)
        except LookupError as e:
            return None

    def __extract(self, path: list, data, history=[]):
        if len(path) == 0:
            return data
        while path[0] == '':
            path = path[1:]

        if type(data) == dict:
            return self.__extractDict(path, data, history)
        elif type(data) == list:
            return self.__extractList(path, data, history)
        else:
            raise ValueError("Invalid type %s at %s"%(type(data), self.separator.join(history)))

    def __extractDict(self, path: list, data, history=[]):
        if len(path) == 0:
            return data
        while path[0] == '':
            path = path[1:]

        try:
            token = path[0]
            got = None

            if token == '*':
                forks = []
                for entry in data.keys():
                    history_f = history.copy()
                    history_f.append(entry)
                    forks.append(self.__extract(path[1:], data[entry], history_f) )
                return forks
            else:
                history.append(token)
                return self.__extract(path[1:], data[path[0]], history)
        except IndexError:
            self.doraise(history)
        except KeyError:
            self.doraise(history)

    def __extractList(self, path: list, data, history=[]):
        if len(path) == 0:
            return data

        token = path[0]
        got = None

        try:
            if re.match("[0-9]$", token):
                got = data[int(token)]
            elif token == '*':
                forks = []
                for i in range(len(data)):
                    history_f = history.copy()
                    history_f.append(str(i))
                    forks.append(self.__extract(path[1:], data[i], history_f) )

                forks = self.__attemptCollate(forks)
                return forks
            else:
                history.append(token)
                raise IndexError("Indax NaN: %s at %s"%(token, self.separator.join( history)) )

            return self.__extract(path[1:], got, history)
        except IndexError:
            self.doraise(history)
        except KeyError:
            self.doraise(history)

    def __attemptCollate(self, forks: list):
        if self.flatten:
            if len(forks) > 0: # We are checking to see if we specifically have an array of arrays
                entry = forks[0]
                if type(entry) == list and len(entry) > 0:
                    forks = [item for result in forks for item in result] # ... otherwise this might flatten incorrectly
        return forks

    def doraise(self, history: list):
        raise LookupError("Invalid lookup key %s" % self.separator.join(history) )
