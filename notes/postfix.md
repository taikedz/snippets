# Postfix reminders

Notes on GNU Postfix

## Configuration

Configuring the mail relay:

Edit /etc/postfix/main.cf

* Ensure myhostname is correct
* set relayhost to the server name

Then send a test email

    cat email_body.txt | mail -s "Email subject" address@one.tld address@two.tld


## Mail queue management

See mail queue

    sudo postqueue -p

Retry deliveries for all items in queue

    sudo postqueue -f

Delete everything

    sudo postsuper -d ALL

